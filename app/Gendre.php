<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gendre extends Model
{
    protected $table = "gendre";
    protected $fillable = ["nama"];
}
