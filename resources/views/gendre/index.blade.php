@extends('layout.master')
@section('judul')
    Halaman List Gendre
@endsection

@section('content')

    <a href="/gendre/create" class="btn btn-success my-3"> Tambah Gendre </a>

    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($gendre as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/gendre/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/gendre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/gendre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
               <h2 style="color: red;">Data Kosong</h2> 
            @endforelse
        </tbody>
    </table>
@endsection