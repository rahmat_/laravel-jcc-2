@extends('layout.master')
@section('judul')
    Halaman Tambah Gendre
@endsection

@section('content')
    <form action="/gendre" method="post">
        @csrf
        <div class="form-group">
            <label>Nama Gendre</label>
            <input type="text" class="form-control" name="nama">
        </div>

        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection