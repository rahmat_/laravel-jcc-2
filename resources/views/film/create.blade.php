@extends('layout.master')
@section('judul')
    Halaman Tambah Film
@endsection

@section('content')
    <form action="/film" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Judul Film</label>
            <input type="text" class="form-control" name="judul">
        </div>

        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Ringkasan Film</label>
            <textarea class="form-control" name="ringkasan"></textarea>
        </div>

        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Tahun</label>
            <input type="number" class="form-control" name="tahun">
        </div>

        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        {{-- <div class="form-group">
            <label>Poster Film</label>
            <textarea class="form-control" name="poster"></textarea>
        </div>

        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}

        <div class="form-group">
            <label>Gendre Film</label>
            <select name="gendre_id" id="" class="form-control">
                <option value="">--Pilih Gendre--</option>
                @foreach ($gendre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>

        @error('gendre_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Poster Film</label>
            <input type="file" class="form-control" name="poster">
        </div>

        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection